# Development stage
FROM node:14 as development
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Test stage
FROM development as test
CMD ["npm", "test"]

# Production stage
FROM development as production
ENV PORT=8080 \
    ORIGIN=8081 \
    DB_NAME=testdb \
    DB_USER=root \
    DB_PASS= \
    DB_ROOT_PASSWORD= \
    DB_HOST='52.66.179.239' \
    DB_PORT=3306 \
    MYSQL_TAG=5.7 \
    SECRET=secret

# Expose the required port
EXPOSE $PORT

# Command to run the application
CMD ["npm", "start"]
