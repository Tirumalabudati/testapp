module.exports = (sequelize, Sequelize) => {
  const Viewer = sequelize.define("viewer", {
    name: {
      type: Sequelize.STRING,
    },
    gender: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
  });

  return Viewer;
};
