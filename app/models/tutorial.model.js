module.exports = (sequelize, Sequelize) => {
  const Tutorial = sequelize.define("tutorial", {
    title: {
      type: Sequelize.STRING,
    },
    description: {
      type: Sequelize.STRING,
    },
    published: {
      type: Sequelize.BOOLEAN,
    },
    viewerId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: "viewers",
        key: "id",
      },
    },
  });

  return Tutorial;
};
