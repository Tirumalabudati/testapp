module.exports = (sequelize, Sequelize) => {
  const ViewerInteraction = sequelize.define("viewer_interaction", {
    Viewer ID: {
      type: Sequelize.STRING,
    },
    gender: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
  });

  return Viewer;
};
