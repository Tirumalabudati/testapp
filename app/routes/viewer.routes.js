module.exports = (app) => {
  const viewers = require("../controllers/viewer.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.post("/", viewers.create);

  // Retrieve all Tutorials
  router.get("/all", viewers.findAll);

  // Retrieve a single Tutorial with id
  router.get("/:id", viewers.findOne);

  // Update a Tutorial with id
  router.put("/:id", viewers.update);

  // Delete a Tutorial with id
  router.delete("/:id", viewers.delete);

  // Delete all Tutorials
  router.delete("/", viewers.deleteAll);

  app.use("/api/viewers", router);
};
