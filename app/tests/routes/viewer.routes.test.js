
const supertest = require('supertest');
const app = require('../../../server');

describe('viewer Routes', () => {
  let request;
  let server;

  beforeAll((done) => {
    server = app.listen();
    request = supertest.agent(server); // Create a request agent using the app
    done();
  });


  it('should create a new viewer', async () => {
    const response = await request
      .post('/api/viewers')
      .send({
        name: 'john',
        gender: 'male',
        address: "hyderabad"
      });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('id');

    // Store the created tutorial ID for cleanup
    // This assumes that the response body contains the ID of the created tutorial
    const viewerId = response.body.id;

    // Optionally, you can return the tutorial ID to access it in afterEach
    return viewerId;
  });

  it('should retrieve all viewers', async () => {
    const response = await request.get('/api/viewers/all');

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBeTruthy();
  });

  it('should retrieve one viewers', async () => {
    const response = await request.get('/api/viewers/viewerId');

    expect(response.status).toBe(404);
    expect(Array.isArray(response.body)).toBe(false);
  });



  afterAll((done) => {
    // Close the server instance
    if (server) {
      server.close((error) => {
        if (error) {
          console.error('Error closing server:', error);
          done(error); // Mark the test as failed if there's an error closing the server
        } else {
          console.log('Server closed successfully.');
          done();
        }
      });
    } else {
      console.warn('No server instance to close.');
      done();
    }
  });

});
