const request = require('supertest');
const app = require('../../../server'); // Assuming your Express app is exported from app.js
const db = require('../../models');
const Viewer = db.viewers;

describe('viewer Controller', () => {
  beforeAll(async () => {
    await Viewer.destroy({ where: {}, truncate: true });
  });

  afterEach(async () => {
    await Viewer.destroy({ where: {}, truncate: true });
  });

  afterAll(async () => {
    await db.sequelize.close();
  });

  describe('create', () => {
    it('should create a new viewers', async () => {
      const response = await request(app)
        .post('/api/viewers')
        .send({
          name: 'mary',
          gender: 'female',
          address: "hyderabad"
        });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('id');
    });

    it('should return a 400 error if title is missing', async () => {
      const response = await request(app)
        .post('/api/viewers')
        .send({
          gender: 'male',
          address: 'mumbai'
        });

      expect(response.status).toBe(400);
      expect(response.body).toEqual({ message: 'Name can not be empty!' });
    });
  });

  describe('findAll', () => {
    it('should retrieve all viewers', async () => {
      await Viewer.bulkCreate([
        { name: 'viewer 1', gender: 'gender 1', address: 'address 1' },
        { name: 'viewer 2', gender: 'gender 2', address: 'address 2' }
      ]);

      const response = await request(app).get('/api/viewers/all');

      expect(response.status).toBe(200);
      expect(response.body.length).toBe(2);
    });
  });

  describe('findOne', () => {
    it('should retrieve a single viewers by id', async () => {
      const createdViewer = await Viewer.create({
        name: 'messy',
        gender: 'female',
        address: "hyderabad"
      });

      const response = await request(app).get(`/api/viewers/${createdViewer.id}`);

      expect(response.status).toBe(200);
      expect(response.body.name).toBe('messy');
      expect(response.body.gender).toBe('female');
      expect(response.body.address).toBe('hyderabad');
    });

    it('should return a 404 error if viewers is not found', async () => {
      const response = await request(app).get('/api/viewers/999');

      expect(response.status).toBe(404);
      expect(response.body).toEqual({ message: 'Cannot find viewer with id=999.' });
    });
  });

  describe('update', () => {
    it('should update a single viewers by id', async () => {
      const createdViewer = await Viewer.create({
        name: 'blessy',
        gender: 'female',
        address: "hyderabad"
      });

      viewerId = createdViewer.id;

      const response = await request(app)
        .put(`/api/viewers/${viewerId}`)
        .send({
          name: 'jocky',
          gender: 'male',
          address: "secunderabad"
        });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Viewer was updated successfully.');

      // Check if the tutorial was actually updated in the database
      const updatedViewer = await Viewer.findByPk(viewerId);
      expect(updatedViewer.name).toBe('jocky');
      expect(updatedViewer.gender).toBe('male');
      expect(updatedViewer.address).toBe('secunderabad');
    });
    });

});
