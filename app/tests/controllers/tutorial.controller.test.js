const request = require("supertest");
const app = require("../../../server"); // Assuming your Express app is exported from app.js
const db = require("../../models");
const Tutorial = db.tutorials;

describe("Tutorial Controller", () => {
  beforeAll(async () => {
    await Tutorial.destroy({ where: {}, truncate: true });
  });

  afterEach(async () => {
    await Tutorial.destroy({ where: {}, truncate: true });
  });

  afterAll(async () => {
    await db.sequelize.close();
  });

  describe("create", () => {
    it("should create a new tutorial", async () => {
      const response = await request(app).post("/api/tutorials").send({
        title: "Test Title",
        description: "Test Description",
        published: true,
        viewerId: 100,
      });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty("id");
    });

    it("should return a 400 error if title is missing", async () => {
      const response = await request(app).post("/api/tutorials").send({
        description: "Test Description",
        published: true,
        viewerId: 103,
      });

      expect(response.status).toBe(400);
      expect(response.body).toEqual({ message: "Content can not be empty!" });
    });
  });

  describe("findAll", () => {
    it("should retrieve all tutorials", async () => {
      await Tutorial.bulkCreate([
        {
          title: "Tutorial 1",
          description: "Description 1",
          published: true,
          viewerId: 101,
        },
        {
          title: "Tutorial 2",
          description: "Description 2",
          published: false,
          viewerId: 102,
        },
      ]);

      const response = await request(app).get("/api/tutorials/all");

      expect(response.status).toBe(200);
      expect(response.body.length).toBe(2);
    });
  });

  describe("findOne", () => {
    it("should retrieve a single tutorial by id", async () => {
      const createdTutorial = await Tutorial.create({
        title: "Test Title",
        description: "Test Description",
        published: true,
        viewerId: 103,
      });

      const response = await request(app).get(
        `/api/tutorials/${createdTutorial.id}`
      );

      expect(response.status).toBe(200);
      expect(response.body.title).toBe("Test Title");
      expect(response.body.description).toBe("Test Description");
      expect(response.body.viewerId).toBe(103);
      expect(response.body.published).toBe(true);
    });

    it("should return a 404 error if tutorial is not found", async () => {
      const response = await request(app).get("/api/tutorials/999");

      expect(response.status).toBe(404);
      expect(response.body).toEqual({
        message: "Cannot find Tutorial with id=999.",
      });
    });
  });

  describe("update", () => {
    it("should update a single tutorial by id", async () => {
      const createdTutorial = await Tutorial.create({
        title: "Test own Tutorial",
        description: "This is a my own test tutorial",
        published: false,
        viewerId: 103,
      });

      tutorialId = createdTutorial.id;

      const response = await request(app)
        .put(`/api/tutorials/${tutorialId}`)
        .send({
          title: "Updated Test Tutorial",
          description: "This is an updated test tutorial",
          published: true,
          viewerId: 104,
        });

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty(
        "message",
        "Tutorial was updated successfully."
      );

      // Check if the tutorial was actually updated in the database
      const updatedTutorial = await Tutorial.findByPk(tutorialId);
      expect(updatedTutorial.title).toBe("Updated Test Tutorial");
      expect(updatedTutorial.description).toBe(
        "This is an updated test tutorial"
      );
      expect(updatedTutorial.published).toBe(true);
      expect(updatedTutorial.viewerId).toBe(104);
    });
  });
});
