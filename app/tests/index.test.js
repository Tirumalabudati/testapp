const request = require('supertest');
const server = require('../../server');

describe('Server', () => {
  let app;

  beforeAll(() => {
    app = server;
  });

  it('should return a 200 status for the root endpoint', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
  });

});
