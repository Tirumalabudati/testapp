describe('Sample Test', () => {
  it('should test that true === true', () => {
    expect(true).toBe(true)
  })
})

describe('Sample Test false', () => {
  it('should test that false === false', () => {
    expect(false).toBe(false)
  })
})

describe('Sample Test numerical', () => {
  it('should test that 1+3 === 4', () => {
    expect(1+3).toBe(4)
  })
})

describe('Sample Test numerical', () => {
  it('should test that 8-3 === 5', () => {
    expect(8-3).toBe(5)
  })
})
